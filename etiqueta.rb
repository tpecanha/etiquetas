#!/usr/bin/ruby

require 'pry'
#require 'roo'
require 'prawn/labels'
require_relative 'coletar_enderecos.rb'

url_base_place = "https://maps.googleapis.com/maps/api/place/textsearch/json"
url_base_geocode = "http://maps.googleapis.com/maps/api/geocode/json"
local = "hospitais"
sensor = "false"
chave = ""

coletar_enderecos = ColetarEnderecos.new(local, sensor,chave)
all_coord = []
all_ceps = []
all_ceps_string = []
nomes_local_sem_cep = []
nomes_local_com_cep = []
cep_inserido = []
enderecos = []
place = "place"
geocode = "geocode"
cep_regexp = /\d{5}-\d{3}/

prox_pag_token = nil
lat = nil
lng = nil

begin
	coletar_enderecos.coletar_lat_long_estabelecimento(place,url_base_place,prox_pag_token,lat,lng)
	coletar_enderecos.json_retorno["results"].each do |local|
		if (local["formatted_address"].match(cep_regexp))
			nomes_local_com_cep << local["name"].to_s + "\n" + local["formatted_address"]
		else
			nomes_local_sem_cep << local["name"].to_s + "\n" + local["formatted_address"]
		end
	end
	all_coord += coletar_enderecos.json_retorno["results"].
	collect{|r| r["geometry"]["location"]}
	prox_pag_token = coletar_enderecos.json_retorno["next_page_token"]
end while(prox_pag_token)

all_coord.each do |coord|
	p latitude = coord["lat"]
	p longitude = coord["lng"]
	coletar_enderecos.coletar_cep_estabelecimento(geocode,url_base_geocode,prox_pag_token=nil,latitude,longitude)
	adress_component = coletar_enderecos.json_retorno["results"].first["address_components"]
	postal_code = adress_component.find{|x| x["types"].include? "postal_code"}
	cep = postal_code["long_name"] if postal_code
	cep << "-000" if cep && !cep.include?("-")
	all_ceps << cep if cep
end

all_ceps.each do |cep|
	all_ceps_string = cep.to_s 
end

nomes_local_sem_cep.zip(all_ceps).map {|x,y| 
	cep_inserido << "#{x} \n #{y}"
}

enderecos = cep_inserido.zip(nomes_local_com_cep).flatten.compact

# s = Roo::Excel.new("Relojoaria.xls")
# s.default_sheet = s.sheets.first
# arr = []
# @enderecos = []

# s.each("nome" => "NOME", "end" => "ENDEREÇO") {|hash| arr << hash}
# arr.each {|x| @enderecos << x["nome"].to_s + "\n" + x["end"].to_s if x["end"] != nil}
# p @enderecos


Prawn::Labels.generate("enderecos.pdf", enderecos , :type => "Envelope10", :shrink_to_fit => true) do |pdf,destinatario|
	pdf.text destinatario
end





