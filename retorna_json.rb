#!/usr/bin/ruby

require 'rubygems'
require 'json'
require 'net/http'


class RetornaJson

	def parse_json(url_base, template_uri)
		uri = URI.parse(url_base)
		url = uri.merge(template_uri)

		try = 1
		resp = Net::HTTP.get_response(url)
		while (try <= 3 && (resp.code != "200" || JSON.parse(resp.body)["status"] != "OK")) do
			sleep try
			resp = Net::HTTP.get_response(url)
			try += 1
		end
		data = resp.body
		result = JSON.parse(data)
		if result.has_key? 'Error'
			raise "Erro no Web Service"
		end
		return result
	end

end
