#!/usr/bin/ruby

require 'rubygems'
require 'json'
require 'net/http'
require_relative 'retorna_json.rb'

class ColetarEnderecos

	attr_accessor :json_retorno

	def initialize(local,sensor,chave)
		@local = local
		@sensor = sensor
		@chave = chave
	end

	def coletar_endereco(tipo_api,url_base,pag_token,lat,lng)
		if tipo_api === "place"
			template_uri = "?query=#{@local}&sensor=#{@sensor}&key=#{@chave}"
			template_uri << "&pagetoken=#{pag_token}" if pag_token
		elsif tipo_api === "geocode"
			template_uri = "?latlng=#{lat},#{lng}&sensor=#{@sensor}"
		end
		
		retorna_json = RetornaJson.new()
		retorna_json.parse_json(url_base, template_uri)
	end

	def coletar_lat_long_estabelecimento(tipo_api,url_base,prox_pag_token,lat,lng)
		result = coletar_endereco(tipo_api,url_base,prox_pag_token,lat,lng)
		@json_retorno = result
	end

	def coletar_cep_estabelecimento(tipo_api,url_base,prox_pag_token,lat,lng)
		result = coletar_endereco(tipo_api,url_base,prox_pag_token,lat,lng)
		@json_retorno = result
	end
end
